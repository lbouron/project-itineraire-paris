// Importations React
import React, { useEffect, useRef } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import axios from 'axios';
import { MapContainer, TileLayer, Marker, Popup, useMapEvents } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet-routing-machine';
import jsPDF from 'jspdf';

// Importation du css
import '../../css/Itineraires.css';

// Autres importations
import { itineraireparisApiService } from '../../services/itineraireparisApiService';

class EditItineraire extends React.Component {
    constructor(props) {
        super(props);

        this.location = props.location;

        this.state = {
            position: [48.8643713, 2.3300615],
            id: this.location.state.itineraire.id,
            depart: this.location.state.itineraire.depart,
            arrivee: this.location.state.itineraire.arrivee,
            coordonneeDepart: this.location.state.itineraire.coordonneeDepart,
            coordonneeArrivee: this.location.state.itineraire.coordonneeArrivee,
            instructions: [],
            pdf: null
        }
    }

    // Fonction qui permet de déplacer le point de départ
    handleDepartMarkerDrag = (e) => {
        const newDepart = [e.target._latlng.lat, e.target._latlng.lng];

        this.setState({
            coordonneeDepart: newDepart
        });
    }

    // Fonction qui permet de déplacer le point d'arrivée
    handleArriveeMarkerDrag = (e) => { 
        const newArrivee = [e.target._latlng.lat, e.target._latlng.lng];

        this.setState({
            coordonneeArrivee: newArrivee
        });
    }

    // Fonction qui met à jour les instructions de l'itinéraire
    updateInstructions = (newInstructions) => {
        this.setState({
            instructions: newInstructions
        });
    }

    // Fonction qui génère un PDF de l'itinéraire
    createPDF = async () => {
        const { coordonneeDepart, coordonneeArrivee, instructions } = this.state;

        if (coordonneeDepart && coordonneeArrivee && instructions.length > 0) {
            const doc = new jsPDF();

            try {
                const adresseDepart = await this.getAdresseCoordonnees(coordonneeDepart[0], coordonneeDepart[1]);
                const adresseArrivee = await this.getAdresseCoordonnees(coordonneeArrivee[0], coordonneeArrivee[1]);

                // Mise en forme départ
                doc.setFont('helvetica', 'bold');
                doc.text('Point de départ :', 10, 10);
                doc.setFont('helvetica', 'normal');
                const adresseDepartLines = doc.splitTextToSize(adresseDepart, 150);
                doc.text(adresseDepartLines, 55, 10);

                // Mise en forme arrivée
                doc.setFont('helvetica', 'bold');
                doc.text('Point d\'arrivée :', 10, 30);
                doc.setFont('helvetica', 'normal');
                const adresseArriveeLines = doc.splitTextToSize(adresseArrivee, 150);
                doc.text(adresseArriveeLines, 55, 30);

                // Mise en forme itinéraire
                doc.setFont('helvetica', 'bold');
                doc.text('Itinéraire :', 10, 50);
                doc.setFont('helvetica', 'normal');

                let y = 60;
                instructions.forEach((instruction, index) => {
                    doc.text(`${index + 1}. ${instruction.text} (${instruction.distance}, ${instruction.time})`, 10, y);
                    y += 10;
                });
                
                doc.save('itineraire.pdf');

                const pdfBase64 = btoa(doc.output());

                this.setState({ 
                    nomDepart: adresseDepart,
                    nomArrivee: adresseArrivee,
                    pdf: pdfBase64
                });
            } catch (error) {
                console.log('Erreur lors de la récupération des adresses :', error);
            }
        }
    }

    // Fonction qui récupère les adresses du lieu de départ et d'arrivée
    getAdresseCoordonnees = async (lat, lng) => {
        try {
            const response = await axios.get(`https://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lng}&format=json`);
            const adresse = response.data.display_name;
            return adresse;
        } catch (error) {
            throw new Error('Erreur lors de la récupération de l\'adresse.');
        }
    }

    render() {
        const { coordonneeDepart, coordonneeArrivee } = this.state;

        return (
            <main>
                <h1>Modifier l'itinéraire</h1>

                <p>
                    Déplacez les points en les glissant sur la map.<br />
                    Générez le PDF de l'itinéraire, puis sauvegardez le nouvel itinéraire.
                </p>
                
                <MapContainer
                    center={this.state.position}
                    zoom={11}
                    maxZoom={19}
                    style={{ height: 400, width: '100%' }}
                >
                    <TileLayer url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png' />

                    {coordonneeDepart && (
                        <Marker
                            position={coordonneeDepart}
                            draggable={true}
                            onDragend={this.handleDepartMarkerDrag}
                        >
                            <Popup>Point de départ</Popup>
                        </Marker>
                    )}

                    {coordonneeArrivee && (
                        <Marker
                            position={coordonneeArrivee}
                            draggable={true}
                            onDragend={this.handleArriveeMarkerDrag}
                        >
                            <Popup>Point d'arrivée</Popup>
                        </Marker>
                    )}

                    {coordonneeDepart && coordonneeArrivee && (
                        <Routing
                            routeFrom={coordonneeDepart}
                            routeTo={coordonneeArrivee}
                            updateInstructions={this.updateInstructions}
                        />
                    )}
                </MapContainer>
                
                <div className='divButtons'>                    
                    <button onClick={this.createPDF}>Générer le PDF de l'itinéraire</button>

                    <button onClick={() => this.editItineraire()}>Modifier l'itinéraire</button>
                </div>
            </main>
        );
    }

    // Fonction qui modifie l'itinéraire dans la base de données
    editItineraire = async () => {
        try {
            const { id, coordonneeDepart, coordonneeArrivee, pdf } = this.state;

            const adresseDepart = await this.getAdresseCoordonnees(coordonneeDepart[0], coordonneeDepart[1]);
            const adresseArrivee = await this.getAdresseCoordonnees(coordonneeArrivee[0], coordonneeArrivee[1]);

            const URL = `/itineraires/update/${id}`;

            let detailsItineraire = {
                depart: adresseDepart,
                arrivee: adresseArrivee,
                coordonneeDepart: coordonneeDepart,
                coordonneeArrivee: coordonneeArrivee,
                pdf: pdf,
                utilisateurId: sessionStorage.getItem('id')
            }

            console.log(detailsItineraire);

            const modif = await itineraireparisApiService.request({
                url: URL,
                method: 'POST',
                data: detailsItineraire
            });

            if (modif.data.result) {
                window.location.pathname = '/home';
            } else {
                throw new Error('Une erreur est survenue.');
            }
        } catch (error) {
            console.log(error);
        }
    }
}

// Fonction qui génère le trajet entre le point de départ et le point d'arrivée
const Routing = ({ routeFrom, routeTo, updateInstructions }) => {
    const routingControlRef = useRef(null);
    const map = useMapEvents({});

    useEffect(() => {
        const calculateRoute = async () => {
            if (routeFrom && routeTo) {
                if (routingControlRef.current) {
                    routingControlRef.current.setWaypoints([
                        L.latLng(routeFrom[0], routeFrom[1]),
                        L.latLng(routeTo[0], routeTo[1])
                    ]);
                } else {
                    const control = L.Routing.control({
                        waypoints: [
                            L.latLng(routeFrom[0], routeFrom[1]),
                            L.latLng(routeTo[0], routeTo[1])
                        ],
                        routeWhileDragging: true,
                        language: 'fr'
                    }).addTo(map);

                    routingControlRef.current = control;

                    control.on('routesfound', (event) => {
                        const newInstructions = event.routes[0].instructions;
                        updateInstructions(newInstructions);
                    });
                }
            }
        }

        calculateRoute();
    }, [routeFrom, routeTo, map, updateInstructions]);

    return null;
}

export default () => (
    <EditItineraire params={useParams()} location={useLocation()} />
);