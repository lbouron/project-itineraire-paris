// Importations React
import React, { useEffect, useRef } from 'react';
import axios from 'axios';
import { MapContainer, TileLayer, Marker, Popup, useMapEvents } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet-routing-machine';
import jsPDF from 'jspdf';

// Importation du css
import '../../css/Itineraires.css';

// Autres importations
import { itineraireparisApiService } from '../../services/itineraireparisApiService';

export default class NewItineraire extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            position: [48.8643713, 2.3300615],
            itineraire: [],
            depart: null,
            arrivee: null,
            nomDepart: null,
            nomArrivee: null,
            instructions: [],
            pdf: null,
            mapEventHandlerKey: Date.now()
        }
    }

    // Fonction qui récupère les coordonnées des clicks sur la map
    handleMapClick = (e) => {
        const { depart, arrivee } = this.state;

        if (!depart) {
            this.setState({
                depart: [e.latlng.lat, e.latlng.lng ]
            });
        } else if (!arrivee) {
            this.setState({
                arrivee: [e.latlng.lat, e.latlng.lng ]
            });
        }
    }

    // Fonction qui permet de déplacer le point de départ
    handleDepartMarkerDrag = (e) => {
        const newDepart = [e.target._latlng.lat, e.target._latlng.lng];

        this.setState({
            depart: newDepart,
            instructions: []
        });
    }

    // Fonction qui permet de déplacer le point d'arrivée
    handleArriveeMarkerDrag = (e) => {
        const newArrivee = [e.target._latlng.lat, e.target._latlng.lng];

        this.setState({
            arrivee: newArrivee,
            instructions: []
        });
    }

    // Fonction qui réinitialise l'itinéraire
    resetItineraire = () => {
        this.setState({
            depart: null,
            arrivee: null,
            instructions: [],
            mapEventHandlerKey: Date.now()
        });
    }

    // Fonction qui génère un PDF de l'itinéraire
    createPDF = async () => {
        const { depart, arrivee, instructions } = this.state;

        if (depart && arrivee && instructions.length > 0) {
            const doc = new jsPDF();

            try {
                const adresseDepart = await this.getAdresseCoordonnees(depart[0], depart[1]);
                const adresseArrivee = await this.getAdresseCoordonnees(arrivee[0], arrivee[1]);

                // Mise en forme départ
                doc.setFont('helvetica', 'bold');
                doc.text('Point de départ :', 10, 10);
                doc.setFont('helvetica', 'normal');
                const adresseDepartLines = doc.splitTextToSize(adresseDepart, 150);
                doc.text(adresseDepartLines, 55, 10);

                // Mise en forme arrivée
                doc.setFont('helvetica', 'bold');
                doc.text('Point d\'arrivée :', 10, 30);
                doc.setFont('helvetica', 'normal');
                const adresseArriveeLines = doc.splitTextToSize(adresseArrivee, 150);
                doc.text(adresseArriveeLines, 55, 30);

                // Mise en forme itinéraire
                doc.setFont('helvetica', 'bold');
                doc.text('Itinéraire :', 10, 50);
                doc.setFont('helvetica', 'normal');

                let y = 60;
                instructions.forEach((instruction, index) => {
                    doc.text(`${index + 1}. ${instruction.text} (${instruction.distance}, ${instruction.time})`, 10, y);
                    y += 10;
                });
                
                doc.save('itineraire.pdf');

                const pdfBase64 = btoa(doc.output());

                this.setState({ 
                    nomDepart: adresseDepart,
                    nomArrivee: adresseArrivee,
                    pdf: pdfBase64
                });
            } catch (error) {
                console.log('Erreur lors de la récupération des adresses :', error);
            }
        }
    }

    // Fonction qui récupère les adresses du lieu de départ et d'arrivée
    getAdresseCoordonnees = async (lat, lng) => {
        try {
            const response = await axios.get(`https://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lng}&format=json`);
            const adresse = response.data.display_name;
            return adresse;
        } catch (error) {
            throw new Error('Erreur lors de la récupération de l\'adresse.');
        }
    }

    render() { 
        const { depart, arrivee, mapEventHandlerKey } = this.state;
        
        return (
            <main>
                <h1>Créer un nouvel itinéraire</h1>

                <p>
                    Cliquez sur la carte pour choisir votre point de départ et d'arrivée.<br />
                    Vous pouvez déplacer les points en les glissant sur la map.<br />
                    Veuillez générer le PDF en cliquant sur le bouton pour enregistrer votre parcours.
                </p>

                <MapContainer
                    center={this.state.position}
                    zoom={11}
                    maxZoom={19}
                    style={{ height: 400, width: '100%' }}
                >
                    <TileLayer url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png' />

                    <MapEventsHandler key={mapEventHandlerKey} onClick={this.handleMapClick} />

                    {depart && (
                        <Marker
                            position={depart}
                            draggable={true}
                            onDragend={this.handleDepartMarkerDrag}
                        >
                            <Popup>Point de départ</Popup>
                        </Marker>
                    )}

                    {arrivee && (
                        <Marker
                            position={arrivee}
                            draggable={true}
                            onDragend={this.handleArriveeMarkerDrag}
                        >
                            <Popup>Point d'arrivée</Popup>
                        </Marker>
                    )}

                    {depart && arrivee && (
                        <Routing
                            routeFrom={depart}
                            routeTo={arrivee}
                            onInstructions={instructions => this.setState({ instructions })}
                        />
                    )}
                </MapContainer>

                {depart && arrivee ? (
                <div className='divButtons'>
                    {/* {!depart && !arrivee && (
                        <button onClick={this.resetItineraire}>Réinitialiser l'itinéraire</button>
                    )} */}
                    
                    <button onClick={this.createPDF}>Générer le PDF de l'itinéraire</button>

                    <button onClick={() => this.addItineraire()}>Ajouter l'itinéraire</button>
                </div>)
                :
                (<div className='divErrors'>
                    <button>Générer le PDF de l'itinéraire</button>

                    <button>Ajouter l'itinéraire</button>
                </div>)}
            </main>
        );
    }

    // Fonction qui ajoute l'itinéraire dans la base de données
    addItineraire = async () => {
        try {
            const { depart, arrivee, pdf } = this.state;

            const adresseDepart = await this.getAdresseCoordonnees(depart[0], depart[1]);
            const adresseArrivee = await this.getAdresseCoordonnees(arrivee[0], arrivee[1]);

            const URL = `/itineraires/add`;

            let detailsItineraire = {
                depart: adresseDepart,
                arrivee: adresseArrivee,
                coordonneeDepart: depart,
                coordonneeArrivee: arrivee,
                pdf: pdf,
                utilisateurId: sessionStorage.getItem('id')
            }

            const ajout = await itineraireparisApiService.request({
                url: URL,
                method: 'PUT',
                data: detailsItineraire
            });

            if (ajout.data.result) {
                window.location.pathname = '/home';
            } else {
                throw new Error('Une erreur est survenue.');
            }
        } catch (error) {
            console.log(error);
        }
    }
}

// Fonction qui provoque un click sur la map
const MapEventsHandler = ({ onClick }) => {
    useMapEvents({
        click: onClick
    });

    return null;
}

// Fonction qui génère le trajet entre le point de départ et le point d'arrivée
const Routing = ({ routeFrom, routeTo, onInstructions }) => {
    const routingControlRef = useRef(null);
    const map = useMapEvents({});

    useEffect(() => {
        const calculateRoute = () => {
            if (routeFrom && routeTo) {
                if (routingControlRef.current) {
                    routingControlRef.current.setWaypoints([
                        L.latLng(routeFrom[0], routeFrom[1]),
                        L.latLng(routeTo[0], routeTo[1])
                    ]);
                } else {
                    const control = L.Routing.control({
                        waypoints: [
                            L.latLng(routeFrom[0], routeFrom[1]),
                            L.latLng(routeTo[0], routeTo[1])
                        ],
                        routeWhileDragging: true,
                        language: 'fr'
                    }).addTo(map);

                    routingControlRef.current = control;

                    control.on('routesfound', (e) => {
                        const routes = e.routes;
                        if (routes.length > 0) {
                            const instructions = routes[0].instructions;
                            onInstructions(instructions);
                        }
                    });
                }
            }
        }

        calculateRoute();
    }, [routeFrom, routeTo, map, onInstructions]);

    return null;
}