// Importations React
import React from 'react';
import { Link } from 'react-router-dom';

// Importations des components

// Importation du css
import '../css/Home.css';

// Autres importations
import { itineraireparisApiService } from '../services/itineraireparisApiService';

export default class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            userId: sessionStorage.getItem('id'),
            itinerairesByUser: []
        }
    }

    componentDidMount() {
        this.getItinerairesByUser();
    }

    render() {
        return (
            <main>
                <Link className='buttonItineraire' to={{ pathname: '/create-itineraire' }}>
                    <button>+ Créer un itinéraire</button>
                </Link>

                <h1>Liste des itinéraires :</h1>
                
                <table>
                    <caption>Liste des itinéraires</caption>

                    <tr>
                        <th>Lieu de départ</th>
                        <th>Lieu d'arrivée</th>
                        <th>Modifier</th>
                        <th>Supprimer</th>
                        <th>Télécharger le PDF</th>
                    </tr>

                    {this.state.itinerairesByUser.length > 0 ?
                        this.state.itinerairesByUser.map((itineraire, index) =>
                        <tr key={index}>
                            <td>{itineraire.depart}</td>
                            <td>{itineraire.arrivee}</td>

                            <td>
                                <Link
                                    to={{ pathname: `/edit-itineraire/${itineraire.id}` }}
                                    state={{ itineraire: itineraire }}
                                    className='linkItineraire'
                                >
                                    Modifier
                                </Link>
                            </td>

                            <td>
                                <button onClick={() => this.deleteItineraire(itineraire.id)}>
                                    Supprimer
                                </button>
                            </td>

                            <td style={{ textAlign: 'center' }}>
                                <a
                                    href={`data:application/octet-stream;base64,${itineraire.pdf}`}
                                    download={`itineraire_${itineraire.id}.pdf`}
                                >
                                    Télécharger
                                </a>
                            </td>
                        </tr>
                        )
                        :
                        <p>Aucun itinéraire.</p>
                    }
                </table>
            </main>
        );
    }

    // Fonction pour récupérer la liste des itinéraires de l'utilisateur connecté
    getItinerairesByUser = async () => {
        try {
            const URL = `/itineraires/user/${this.state.userId}`;

            const itinerairesByUser = await itineraireparisApiService.request({
                url: URL,
                method: 'GET'
            });

            if (itinerairesByUser.data.result) {                
                this.setState({ itinerairesByUser: itinerairesByUser.data.result });
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction qui supprime un itinéraire de la liste
    deleteItineraire = async (id) => {
        try {
            const URL = `/itineraires/delete/${id}`;

            let itineraire = await itineraireparisApiService.request({
                url: URL,
                method: 'DELETE'
            });

            if (itineraire.data.result) {
                window.location.reload(true);
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }
}