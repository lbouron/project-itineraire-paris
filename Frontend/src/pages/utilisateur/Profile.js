// Importations React
import React from 'react';

// Importation du css
import '../../css/Profile.css';

// Autres importations
import { itineraireparisApiService } from '../../services/itineraireparisApiService';

export default class Profile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editUser: {
                id: null,
                pseudo: null
            },
            isError: {
                pseudo: null
            }
        }
    }

    async componentDidMount() {
        await this.getUser();
    }

    render() {
        return (
            <main>
                <h1>Mon profil</h1>

                <form className='formProfile' onSubmit={this.editPseudo}>
                    <input
                        type='text'
                        id='pseudo'
                        placeholder='Votre pseudo'
                        defaultValue={this.state.editUser.pseudo}
                        className={this.state.isError.pseudo && 'errorProfile'}
                        onChange={element => this.setState({
                            editUser: {
                                ...this.state.editUser,
                                pseudo: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.pseudo &&
                    <p>Le pseudo ne peut pas être vide.</p>}

                    <input
                        type='submit'
                        value='Modifier le pseudo'
                    />
                </form>
            </main>
        );
    }

    // Fonction pour récupérer les infos de l'utilisateur
    getUser = async () => {
        try {
            const userInfos = await itineraireparisApiService.request({
                url: '/userInfo',
                method: 'GET'
            });

            if (userInfos.statusText === 'OK') {
                this.setState({
                    editUser: {
                        ...this.state.editUser,
                        id: userInfos.data.id,
                        pseudo: userInfos.data.pseudo
                    }
                });
            } else {
                throw new Error();
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction pour modifier le pseudo
    editPseudo = async (e) => {
        e.preventDefault();

        try {
            const URL = `/users/update/${this.state.editUser.id}`;

            if (this.checkInputs()) {
                let pseudo = await itineraireparisApiService.request({
                    url: URL,
                    method: 'POST',
                    data: {
                        "pseudo": this.state.editUser.pseudo
                    }
                });

                if (pseudo.data.result) {
                    sessionStorage.setItem('pseudo', this.state.editUser.pseudo);
                    
                    window.location.pathname = '/home';
                } else {
                    throw new Error();
                }
            } else {
                console.log('Erreur dans les champs.');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction pour vérifier les inputs
    checkInputs = () => {
        let { isError } = this.state;
        let { editUser } = this.state;
        let inputsCorrects = true;

        for (const [key, value] of Object.entries(editUser)) {
            switch (key) {
                case 'pseudo':
                    isError[key] = value == null || value.length < 1;
                    break;
                default:
                    break;
            }
        }
        this.setState({ isError });

        // Retour false si un des champs est incorrect
        for (const [key, value] of Object.entries(isError)) {
            if (value) inputsCorrects = false;
            if (key) { }
        }

        return inputsCorrects;
    }
}