# Itinéraire Paris - FrontEnd

## Lancement
Taper `npm start` pour démarrer le FrontEnd. Il est possible qu'il demande à se lancer sur un autre port, car le port 3000 sera utilisé par l'API.

## Connexion
Après que l'API ait été démarrée pour la première fois, deux utilisateurs (admin et user) sont créés :

`Admin`
pseudo : admin
mdp : test

`User`
pseudo : usertest
mdp : test