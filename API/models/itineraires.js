const Sequelize = require('sequelize');
const db        = require('../services/sequelize');
const models    = require('./index');

let modelDefinition = {
    id: {
        type: Sequelize.DataTypes.BIGINT,
        primaryKey: true,
        unique: true,
        allowNull: false,
        autoIncrement: true
    },
    depart: {
        type: Sequelize.DataTypes.TEXT,
        allowNull: false
    },
    coordonneeDepart: {
        type: Sequelize.DataTypes.ARRAY(Sequelize.DataTypes.FLOAT),
        allowNull: false
    },
    arrivee: {
        type: Sequelize.DataTypes.TEXT,
        allowNull: false
    },
    coordonneeArrivee: {
        type: Sequelize.DataTypes.ARRAY(Sequelize.DataTypes.FLOAT),
        allowNull: false
    },
    pdf: {
        type: Sequelize.DataTypes.TEXT,
        allowNull: true
    }
}

let modelOptions = {
    freezeTableName: true,
    paranoid: true,
    timestamps: true
}

let itinerairesModel = db.define('itineraires', modelDefinition, modelOptions);

itinerairesModel.belongsTo(models.utilisateurs);

module.exports = itinerairesModel;