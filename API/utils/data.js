const sequelize = require('../services/sequelize');
const models    = require('../models');

async function importDefaultData() {
    await sequelize.sync();
    await importUtilisateurs();
    await importTokens();
}

async function importUtilisateurs() {
    if (!await models.utilisateurs.count()) {
        await models.utilisateurs.bulkCreate([
            {
                id: 'a0a4defa-f2ce-4963-9efc-86c716040cd4',
                pseudo: 'admin',
                password: '$2y$10$TDOunZGTy56KVhbfQCh4UOMMVg3gyerNPyTl5ZKUmmsXVK4OG10cG',
                isAdmin: true
            },
            {
                id: '9886a432-b52a-4eb5-b1f4-2d55a1341df3',
                pseudo: 'usertest',
                password: '$2y$10$TDOunZGTy56KVhbfQCh4UOMMVg3gyerNPyTl5ZKUmmsXVK4OG10cG',
                isAdmin: false
            }
        ]);
    }
}

async function importTokens() {
    if (!await models.tokens.count()) {
        await models.tokens.bulkCreate([
            {
                id: 1,
                token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImZmZmQ0ZjMwLTIzNzktMTFlZS05ODczLTRkNjlmOWQxNzIxNiIsInBzZXVkbyI6ImFkbWluIiwicGFzc3dvcmQiOiIkMmEkMTAkb296aUgweXpDYm1oRHhrZXVKc1hZT1o3STltR3RBNlUxcGtXcklxMWExRGRweXdmVmtRa1ciLCJpc0FkbWluIjpmYWxzZSwiY3JlYXRlZEF0IjoiMjAyMy0wNy0xNlQwMTo0MjoxOC43OTBaIiwidXBkYXRlZEF0IjoiMjAyMy0wNy0xNlQwMTo0MjoxOC43OTBaIiwiZGVsZXRlZEF0IjpudWxsLCJpYXQiOjE2ODk0NzE4MzAsImV4cCI6MTY4OTU1ODIzMH0.nibsDwXSCln6GSsNcePIi-A4GGMDy9umpNkXEzNdMkg',
                utilisateurId: 'a0a4defa-f2ce-4963-9efc-86c716040cd4'
            },
            {
                id: 2,
                token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjhhMGFjZDEwLTIzN2EtMTFlZS05ODczLTRkNjlmOWQxNzIxNiIsInBzZXVkbyI6InVzZXJ0ZXN0IiwicGFzc3dvcmQiOiIkMmEkMTAkcTgvUE5ZQjlOTHJsOU1FU1lkQ3lhLmRsSzVmdUVhOWQ2M2dSWUdZTlN0Tm5tZG9rYTl5dS4iLCJpc0FkbWluIjpmYWxzZSwiY3JlYXRlZEF0IjoiMjAyMy0wNy0xNlQwMTo0NjoxMC40MDFaIiwidXBkYXRlZEF0IjoiMjAyMy0wNy0xNlQwMTo0NjoxMC40MDFaIiwiZGVsZXRlZEF0IjpudWxsLCJpYXQiOjE2ODk0NzE5NzcsImV4cCI6MTY4OTU1ODM3N30.9y9pgpysfLpcxT1lbARxbh9BlQTiVcns5GPSBU41ebU',
                utilisateurId: '9886a432-b52a-4eb5-b1f4-2d55a1341df3'
            }
        ]);
    }
}

module.exports = {
    importDefaultData
}